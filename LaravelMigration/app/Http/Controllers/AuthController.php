<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function send_register(Request $request)
    {
        // return $request;
        $namaDepan = $request->input('name');
        $namaBelakang = $request->input('lastname');

        return view('welcome',['namaDepan'=>$namaDepan, 'namaBelakang'=>$namaBelakang]);
    }
}
